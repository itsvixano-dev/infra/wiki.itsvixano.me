---
hide:
  - navigation
---

[:material-arrow-u-left-bottom: Back to the overview](index.md)

# Install LineageOS on {{ __device.vendor__ }} {{ __device.name__ }}

!!! warning
    The provided instructions are for LineageOS {{ __device.current_version__ }}. These will only work if you follow every section and step precisely. Do **not** continue after something fails!

!!! note
    Make sure you have read the [Getting started](../../index.md) page before proceeding with the installation.

## Download the required assets

### LineageOS

* Go to the <a href="https://github.com/ItsVixano-releases/LineageOS_{{ __device.codename__ }}/releases/latest" target="_blank">Download Portal</a> and download the following assets:
    * `lineage-{{ __device.current_version__ }}-xxxxxxxx-UNOFFICIAL-{{ __device.codename__ }}.zip`
{{ __device.recovery_images__ }}

!!! note
    You can verify the authencity of these builds by following [this](../../verifying_builds.md) page.

!!! warning
    If you want to install Google Apps add-on package, you can download it from [here](../../gapps.md) (use the {{ __device.arch__ }} architecture). This add-on needs to be installed **before** booting into LineageOS for the first time!

{{ __download_vendor_firmware__ }}

## Install Lineage Recovery using fastboot

* Reboot your device into fastboot mode by holding down the appropriate key combination.
* Open a `ADB & Fastboot` tools window on your PC and flash the `Lineage Recovery` assets you downloaded before:

```bash
{{ __recovery_images_flash__ }}
```

* Reboot your device into recovery mode by holding the appropriate key combination.

{{ __flash_vendor_firmware__ }}

## Install LineageOS from recovery

* On the `Lineage Recovery` home screen, tap `Factory reset`, then `Format data/factory reset` and press `Format data`. This will delete all your device's data and remove encryption from an older ROM.
* On the `Lineage Recovery` home screen, tap `Apply update`, then `Apply from ADB` for starting the sideload service.
* Open a `ADB & Fastboot` tools window on your PC and flash the `LineageOS` install package you downloaded before:

```bash
$ adb sideload lineage-{{ __device.current_version__ }}-xxxxxxxx-UNOFFICIAL-{{ __device.codename__ }}.zip
```

!!! tip
    After the package is installed, recovery will _may_ inform you that reboot to recovery is required to install add-ons. In case you want to do that, please select `Yes`, otherwise `No`.

!!! tip
    Normally, adb will report `Total xfer: 1.00x`, but in some cases, even if the process succeeds the output will stop at 47% and report `adb: failed to read command: Success`. In some cases it will report `adb: failed to read command: No error` or `adb: failed to read command: Undefined error: 0` which is also fine.

## Install Add-Ons

!!! note
    If you don’t want to install any add-on (such as Google Apps), you can skip this whole section!

* On the `Lineage Recovery` home screen, tap `Apply update`, then `Apply from ADB` for starting the sideload service.
* Open a `ADB & Fastboot` tools window on your PC and flash the add-on package you downloaded before:

```bash
$ adb sideload add-on.zip
```

!!! note
    When presented with a screen that says Signature verification failed, click `Yes`. It is expected as add-on packages aren’t signed with the same keys!

## All set!

* On the `Lineage Recovery` home screen, tap `Reboot system now`.
* If everything went well, you will be greeted by the infamous LineageOS boot animation :D.
