---
hide:
  - navigation
---

[:material-arrow-u-left-bottom: Back to the overview](index.md)

# Update LineageOS on {{ __device.vendor__ }} {{ __device.name__ }}

## Using the LineageOS Updater app (to newer LineageOS builds)

* Open Settings, navigate to `System`, then `Updater`.
* Click the Refresh Icon in the top right corner.
* Choose which update you’d like and press `Download`.
* When the download completes, click “Install”. Your device will reboot to recovery and install the update, then reboot to the updated installation.
