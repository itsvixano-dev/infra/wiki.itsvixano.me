---
hide:
  - navigation
---

[:material-arrow-u-left-bottom: Back to the overview](index.md)

# Upgrade LineageOS on {{ __device.vendor__ }} {{ __device.name__ }}

## Manually upgrading LineageOS (to higher LineageOS version)

!!! note
    The updater app does not support upgrades from one version of LineageOS to another, and will block installation to any update for a different version. Upgrading manually requires similar steps to installing LineageOS for the first time.

!!! warning
    If you previously had a Google Apps add-on package installed on your device, you will need to install an updated package **before** booting Android for the first time! If you did not have Google Apps installed, you will need to wipe the **Data** partition (or perform a factory reset) to install it. The updated Google Apps add-on package can be found [here](../../gapps.md) (use the {{ __device.arch__ }} architecture).

* Go to the <a href="https://github.com/ItsVixano-releases/LineageOS_{{ __device.codename__ }}/releases/latest" target="_blank">Download Portal</a> and download the newest version.
* Reboot your device into recovery mode by holding the appropriate key combination.
* On the `Lineage Recovery` home screen, tap `Apply update`, then `Apply from ADB` for starting the sideload service.
* Open a `ADB & Fastboot` tools window on your PC and flash the `LineageOS` install package you downloaded before:

```bash
$ adb sideload lineage-{{ __device.current_version__ }}-xxxxxxxx-UNOFFICIAL-{{ __device.codename__ }}.zip
```

!!! note
    After the package is installed, recovery will _may_ inform you that reboot to recovery is required to install add-ons. In case you want to do that, please select `Yes`, otherwise `No`.

## Installing Add-Ons

!!! note
    If you don’t want to install any add-on (such as Google Apps), you can skip this whole section!

* On the `Lineage Recovery` home screen, tap `Apply update`, then `Apply from ADB` for starting the sideload service.
* Open a `ADB & Fastboot` tools window on your PC and flash the add-on package you downloaded before:

```bash
$ adb sideload add-on.zip
```
!!! note
    When presented with a screen that says Signature verification failed, click `Yes`. It is expected as add-on packages aren’t signed with the same keys!

## All set!

* On the `Lineage Recovery` home screen, tap `Reboot system now`.
* If everything went well, you will be greeted by the infamous LineageOS boot animation :D.
