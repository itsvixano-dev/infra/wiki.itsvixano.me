---
hide:
  - navigation
---

# Devices

Devices marked with a strikethrough text are no longer supported. Their wiki pages will remain for reference only.
