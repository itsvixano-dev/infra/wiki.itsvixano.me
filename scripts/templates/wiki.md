---
hide:
  - navigation
---

[:material-arrow-u-left-bottom: Devices](../index.md)

# {{ __device.vendor__ }} {{ __device.name__ }} (`{{ __device.codename__ }}`)

{{ __device_unsupported__ }}

| **Specs**         |                      |
| :---------------- | -------------------: |
| SoC               | {{ __device.soc__ }} |
| Architecture      | {{ __device.arch__ }} |
| RAM               | {{ __device.ram__ }} |
| Storage           | {{ __device.storage__ }} |
| GPU               | {{ __device.gpu__ }} |
| Relase Date       | {{ __device.release_date__ }} |

| **LineageOS info** |                                  |
| :----------------- | -------------------------------: |
| Current version    | {{ __device.current_version__ }} |
| Kernel version     | {{ __device.kernel_version__ }}  |
{{ __device.previous_versions__ }}

## Supported variants / models

{{ __device.models__ }}

## Downloads

<a href="https://github.com/ItsVixano-releases/LineageOS_{{ __device.codename__ }}/releases/latest" target="_blank">Get the builds here</a>

## Guides

* [Installation](install.md)
* [Update to a newer build of the same LineageOS version](update.md)
{{ __upgrade_guide__ }}

{{ __special_device_notes__ }}

## Special boot modes

{{ __device.boot_modes__ }}

## Find help online

You can find assistance with LineageOS on [our Telegram chat](https://t.me/giostuffs) or in [#giostuffs:matrix.org on Matrix](https://matrix.to/#/#giostuffs:matrix.org).

## Report a bug

If you’d like to report a bug, follow the instructions [here](../../troubleshooting.md).
