#!/usr/bin/env python3
# Copyright (C) 2024-2025 Giovanni Ricca
# SPDX-License-Identifier: Apache-2.0

import os
import shutil

import yaml

# Paths to the template files
template_files = {
    'install': 'scripts/templates/install.md',
    'update': 'scripts/templates/update.md',
    'upgrade': 'scripts/templates/upgrade.md',
    'wiki': 'scripts/templates/wiki.md',
    'devices_list': 'scripts/templates/devices_list.md',
}

# Path to the devices directory
devices_directory = '_data/devices/'

# Path to the output directory
output_directory = 'docs/devices/'


def replace_contents(contents, replacements):
    for placeholder, value in replacements.items():
        contents = contents.replace(placeholder, value)
    return contents


def generate_device_markdown(template_file, output_markdown, content_edits):
    # Iterate over each device file in the devices directory
    for device_file in os.listdir(devices_directory):
        # Read the device data from the YAML file
        with open(os.path.join(devices_directory, device_file), 'r') as f:
            device_data = yaml.safe_load(f)

        # Create the output directory for the device
        device_directory = os.path.join(
            output_directory, device_data['codename']
        )
        os.makedirs(device_directory, exist_ok=True)

        # Copy the template file to the device directory
        output_file = os.path.join(device_directory, output_markdown)
        shutil.copyfile(template_file, output_file)

        # Read the contents of the template file
        with open(output_file, 'r') as f:
            contents = f.read()

        # Modify the contents of the file
        contents = content_edits(contents, device_data)

        # Write the modified contents back to the output file
        with open(output_file, 'w') as f:
            f.write(contents)


def install_contents(contents, device_data):
    # Vars
    current_version = (
        device_data.get('current_version', '')
        if '.' in device_data.get('current_version', '')
        else device_data.get('current_version', '') + '.0'
    )
    recovery_images = '\n'.join(
        [
            '    * `' + recovery_image + '.img`'
            for recovery_image in device_data.get('recovery_images', [])
        ]
    )
    recovery_images_flash = '\n'.join(
        [
            '$ fastboot flash ' + recovery_image + ' ' + recovery_image + '.img'
            for recovery_image in device_data.get('recovery_images', [])
        ]
    )
    firmware_download = f"""### Vendor firmware

* Download the latest vendor firmware available for your device:

| Firmware | Download |
| :--- | ---: |
{"\n".join(["| " + vendor_firmware.get("firmware") + " | [here](" + "https://github.com/ItsVixano-releases/LineageOS_extra/releases/download/xiaomi_firmwares/" + vendor_firmware.get("firmware_zip") + ") |" for vendor_firmware in device_data.get("vendor_firmwares", [])])}"""
    firmware_flash = """## Install vendor firmware from recovery

* On the `LineageOS Recovery` home screen, tap `Apply update`, then `Apply from ADB` for starting the sideload service.
* Open a `ADB & Fastboot` tools window on your PC and flash the `vendor firmware` install package you downloaded before:

```bash
$ adb sideload vendor_firmware.zip
```

!!! note
    When presented with a screen that says Signature verification failed, click `Yes`. It is expected as vendor firmware packages aren’t signed with the same keys!"""

    replacements = {
        '{{ __device.vendor__ }}': device_data.get('vendor', ''),
        '{{ __device.name__ }}': device_data.get('name', ''),
        '{{ __device.codename__ }}': device_data.get('codename', ''),
        '{{ __device.arch__ }}': device_data.get('arch', ''),
        '{{ __device.current_version__ }}': current_version,
        '{{ __device.recovery_images__ }}': recovery_images,
        '{{ __recovery_images_flash__ }}': recovery_images_flash,
        '{{ __download_vendor_firmware__ }}': (
            firmware_download
            if len(device_data.get('vendor_firmwares', [])) != 0
            else ''
        ),
        '{{ __flash_vendor_firmware__ }}': (
            firmware_flash
            if len(device_data.get('vendor_firmwares', [])) != 0
            else ''
        ),
    }

    return replace_contents(contents, replacements)


def update_contents(contents, device_data):
    replacements = {
        '{{ __device.vendor__ }}': device_data.get('vendor', ''),
        '{{ __device.name__ }}': device_data.get('name', ''),
    }

    return replace_contents(contents, replacements)


def upgrade_contents(contents, device_data):
    # Vars
    current_version = (
        device_data.get('current_version', '')
        if '.' in device_data.get('current_version', '')
        else device_data.get('current_version', '') + '.0'
    )

    replacements = {
        '{{ __device.vendor__ }}': device_data.get('vendor', ''),
        '{{ __device.name__ }}': device_data.get('name', ''),
        '{{ __device.codename__ }}': device_data.get('codename', ''),
        '{{ __device.arch__ }}': device_data.get('arch', ''),
        '{{ __device.current_version__ }}': current_version,
    }

    return replace_contents(contents, replacements)


def wiki_contents(contents, device_data):
    # Vars
    unsupported_msg = '!!! warning\n    The device is no longer maintained.'

    previous_versions = device_data.get('previous_versions', '')
    previous_versions_split = previous_versions.split()
    formatted_versions = '<br>'.join(previous_versions_split)
    previous_versions_msg = (
        f'| Previously supported versions | {formatted_versions} |'
    )

    kernel_version = device_data.get('kernel_version', '')
    kernel_string = kernel_version + f' ([source code]({device_data.get('kernel_src', '')}))'

    upgrade_msg = f'* [Upgrade to a higher build of LineageOS (eg. lineage-{previous_versions_split[-1] if previous_versions_split else ""} -> lineage-{device_data.get("current_version", "")})](upgrade.md)'

    special_msg = f'## Special device notes\n\n{"\n".join(["* " + special_note for special_note in device_data.get("special_notes", [])])}'

    models_list = '\n'.join(
        ['* ' + model for model in device_data.get('models', [])]
    )

    boot_modes = '\n'.join(
        ['* ' + boot_mode for boot_mode in device_data.get('boot_modes', [])]
    )

    replacements = {
        '{{ __device.vendor__ }}': device_data.get('vendor', ''),
        '{{ __device.name__ }}': device_data.get('name', ''),
        '{{ __device.codename__ }}': device_data.get('codename', ''),
        '{{ __device.soc__ }}': device_data.get('soc', ''),
        '{{ __device.gpu__ }}': device_data.get('gpu', ''),
        '{{ __device.ram__ }}': device_data.get('ram', ''),
        '{{ __device.arch__ }}': device_data.get('arch', ''),
        '{{ __device.storage__ }}': device_data.get('storage', ''),
        '{{ __device.release_date__ }}': device_data.get('release_date', ''),
        '{{ __device.kernel_version__ }}': kernel_string,
        '{{ __device.current_version__ }}': device_data.get(
            'current_version', ''
        ),
        '{{ __device.previous_versions__ }}': (
            previous_versions_msg
            if len(device_data.get('previous_versions', '')) != 0
            else ''
        ),
        '{{ __upgrade_guide__ }}': (
            upgrade_msg
            if len(device_data.get('previous_versions', '')) != 0
            else ''
        ),
        '{{ __device.models__ }}': models_list,
        '{{ __device.boot_modes__ }}': boot_modes,
        '{{ __device_unsupported__ }}': (
            unsupported_msg
            if device_data.get('is_maintained', '') == 'false'
            else ''
        ),
        '{{ __special_device_notes__ }}': (
            special_msg
            if len(device_data.get('special_notes', [])) != 0
            else ''
        ),
    }

    return replace_contents(contents, replacements)


def generate_devices_md():
    # Create a dictionary to store the devices by vendor
    devices_by_vendor = {}

    # Iterate over each device file in the devices directory
    for device_file in os.listdir(devices_directory):
        # Read the device data from the YAML file
        with open(os.path.join(devices_directory, device_file), 'r') as f:
            device_data = yaml.safe_load(f)

        # Get the vendor and device string
        vendor = device_data.get('vendor', '')
        if device_data.get('is_maintained', '') == 'true':
            device_string = f"* [~~{device_data.get('name', '')} (`{device_data.get('codename', '')}`)~~]({device_data.get('codename', '')}/index.md)"
        else:
            device_string = f"* [{device_data.get('name', '')} (`{device_data.get('codename', '')}`)]({device_data.get('codename', '')}/index.md)"

        # Add the device to the correct vendor's list in the dictionary
        if vendor in devices_by_vendor:
            devices_by_vendor[vendor].append(
                (device_data.get('codename', ''), device_string)
            )
        else:
            devices_by_vendor[vendor] = [
                (device_data.get('codename', ''), device_string)
            ]

    # Sort the devices by codename for each vendor
    for vendor in devices_by_vendor:
        devices_by_vendor[vendor].sort()

    # Create the devices_md_contents list
    shutil.copy(
        template_files['devices_list'],
        os.path.join(output_directory, 'index.md'),
    )

    # Append the devices to the index.md file
    with open(os.path.join(output_directory, 'index.md'), 'a') as f:
        for vendor, devices in devices_by_vendor.items():
            f.write(f'\n## {vendor}\n\n')
            for _, device in devices:
                f.write(device + '\n')


# Generate the device files
generate_device_markdown(template_files['install'], 'install.md', install_contents)  # fmt: skip
generate_device_markdown(template_files['update'], 'update.md', update_contents)  # fmt: skip
generate_device_markdown(template_files['upgrade'], 'upgrade.md', upgrade_contents)  # fmt: skip
generate_device_markdown(template_files['wiki'], 'index.md', wiki_contents)  # fmt: skip
generate_devices_md()  # fmt: skip
