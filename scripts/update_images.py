#!/usr/bin/env python3
# Copyright (C) 2024 Giovanni Ricca
# SPDX-License-Identifier: Apache-2.0
# deps: libwebp, python-pillow

from pathlib import Path
from urllib.request import urlretrieve as request

from PIL import Image

# vars
webp_file = 'docs/images/logo.webp'
favicon_file = 'docs/images/favicon.ico'
url = 'https://avatars.githubusercontent.com/u/50358281'


# defs
def resize_image(input_path, output_path, size):
    with Image.open(input_path) as img:
        format = Path(output_path).suffix[1:].upper()
        img = img.resize(size, Image.LANCZOS)
        img.save(output_path, format=format)


# Downloading the pfp and resizing it
request(url, webp_file)
resize_image(webp_file, webp_file, (200, 200))
resize_image(webp_file, favicon_file, (16, 16))
