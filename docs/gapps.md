---
hide:
  - navigation
---

# Google Apps

Google apps are the proprietary Google-branded applications that come pre-installed with most Android devices, such as the Play Store, Gmail, Maps, etc. The Google apps are not required to boot or run LineageOS, however many users find them beneficial to take full advantage of the Android ecosystem.

These apps have been packaged by developers independent of LineageOS, and download links have been provided for your convenience only.

## Installation

If you choose to install Google apps they must be installed via recovery immediately after installing LineageOS. Exact steps vary, and as such, you should see your device’s installation guide here for specific instructions.

!!! warning
    If you reboot into LineageOS before installing Google apps, you must factory reset and then install them, otherwise expect crashes.

    This also applies when you experience issues and want to try an older or other package of these apps.

## Downloads

These packages are only dependent on your OS version and architecture, which can be found on each device specific info page in this wiki [Device overview](devices/index.md).

| Version                      | MindTheGapps Link     |
| :--------------------------- | --------------------: |
| LineageOS 22.1 (Android 15)  | \| <a href="https://github.com/MindTheGapps/15.0.0-arm/releases/latest" target="_blank">ARM</a> \| <a href="https://github.com/MindTheGapps/15.0.0-arm64/releases/latest" target="_blank">ARM64</a> \| |
| LineageOS 21.0 (Android 14)  | \| <a href="https://github.com/MindTheGapps/14.0.0-arm/releases/latest" target="_blank">ARM</a> \| <a href="https://github.com/MindTheGapps/14.0.0-arm64/releases/latest" target="_blank">ARM64</a> \| |
| LineageOS 20.0 (Android 13)  | \| <a href="https://github.com/MindTheGapps/13.0.0-arm/releases/latest" target="_blank">ARM</a> \| <a href="https://github.com/MindTheGapps/13.0.0-arm64/releases/latest" target="_blank">ARM64</a> \| |
| LineageOS 19.1 (Android 12L) | \| <a href="https://github.com/MindTheGapps/12.1.0-arm/releases/latest" target="_blank">ARM</a> \| <a href="https://github.com/MindTheGapps/12.1.0-arm64/releases/latest" target="_blank">ARM64</a> \| |
| LineageOS 18.1 (Android 11)  | \| <a href="https://github.com/MindTheGapps/11.0.0-arm/releases/latest" target="_blank">ARM</a> \| <a href="https://github.com/MindTheGapps/11.0.0-arm64/releases/latest" target="_blank">ARM64</a> \| |
