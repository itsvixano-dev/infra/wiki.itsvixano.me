---
hide:
  - navigation
---

# Introduction

Greetings and welcome!

## Getting started

Before going forward, make sure you have met these requirements:

* Make sure your PC has the latest [`ADB & Fastboot tools`](https://developer.android.com/studio/releases/platform-tools) version installed as well as the fastboot drivers.
* Make sure your device has the bootloader unlocked.
* Make sure you have made a backup of **EVERYTHING** present on your phone.
* These builds include the support for OTA updates.
* Custom kernels are **NOT SUPPORTED, do NOT flash them**.
* Flash LineageOS **ONLY** with Lineage Recovery (The installation guide will tell you how to flash it).
* Apart from the vanilla LineageOS source, I also apply some patches on my own, which can be found [here](https://gitlab.com/itsvixano-dev/android/lineageos-personal).
* Most of the device sources used for my builds can be found [here](https://gitlab.com/itsvixano-dev/android/lineageos-personal/rootdir) (Take a look at the local manifests folder and branches :D).

## Support channels

I am active on both XDA threads and my own [Telegram group](https://t.me/giostuffs) or [Matrix bridge](https://matrix.to/#/#giostuffs:matrix.org) if you want to ask any questions or resolve some problems.

<div style="text-align: right">
    <a href="devices" class="md-button">Next step</a>
</div>
