---
hide:
  - navigation
---

# Verifying Build Authenticity

!!! note
    To go ahead with the verification, `git`, `python3-pip`, and `python3` are required.

Download the verifier and install its dependencies:

```bash
$ git clone https://github.com/ItsVixano-releases/update_verifier
$ cd update_verifier
$ python3 -m venv .venv
$ source .venv/bin/activate
$ pip3 install -r requirements.txt
```

Check the signature of the downloaded ZIP file:

```bash
$ python3 update_verifier.py lineageos_pubkey /path/to/zip
```

If the script reports `verified successfully`, the ZIP file signature is valid.
