---
hide:
  - navigation
---

# Licenses

ItsVixano LineageOS Wiki is licensed under [`CC BY 4.0`](https://gitlab.com/itsvixano-dev/infra/wiki.itsvixano.me/-/blob/master/LICENCE?ref_type=heads).

This Wiki makes use of several open-source tools and resources. We acknowledge and thank the creators of these projects for their contributions to the open-source community.

| Project / Resource | License |
| :----------------- | ------: |
| LineageOS Wiki | [`CC BY-SA 3.0`](https://github.com/LineageOS/lineage_wiki/blob/main/licenses/LICENSE) |
| Material for MkDocs | [`MIT`](https://github.com/squidfunk/mkdocs-material/blob/master/LICENSE) |
| Catppuccin Palettes | [`MIT`](https://github.com/catppuccin/palette/blob/main/LICENSE) |
