---
hide:
  - navigation
---

[:material-arrow-u-left-bottom: Back to the overview](../devices/xaga/index.md)

# Flashing `CFW` on `xaga` and unbrick it

## Getting Started

As we have seen in the past ["`begonia` CFW Megathread"](https://xdaforums.com/t/guide-info-psa-redmi-note-8-pro-megathread-cfw.4056527/), MTK devices are pretty easy to brick. All it takes is one wrong move and you are **FUCKED!**.

The same concept of a brick also applies to `xaga` devices ["Example of what a brick looks like"](https://imgur.com/a/zMw2udb).

!!! note
    If you are already in this state and have not flashed the engineering `Preloader` image, the only way out is to visit an authorised Xiaomi Center.

### So what do we do to avoid a brick?

It's not that easy to avoid bricking these devices. Just follow the instructions and don't do anything stupid.

However, by flashing the engineering `Preloader` image, you can save yourself a lot of headaches.

### Why do we need to flash the engineering `Preloader` image and how it works?

Usually, MTK devices follow this boot pattern:

    `Boot ROM` -> `Preloader` -> `TrustZone (TZ)` -> `GenieZone (GZ)` -> `Little Kernel (lk)` -> `Kernel`

With the stock `Preloader`, nothing unsual happens, however, after flashing the engineering `Preloader` image, (with ever boot) the `Preloader` exposes an insecure `VCOM` port with `SLA` (Serial Link Authentication) and `DAA` (Download Agent Authentication) checks disabled, allowing you to flash images with `SP Flash tool V6` without worrying about having an authorised Mi account.

If you use the stock `Preloader` image, the only "download" mode you can access in case of a brick is `Boot ROM` (which is burnt into the SoC). This requires an authorised Mi account to access and write partitions from it.

There is currently no way to bypass these checks on `xaga`, as `Boot ROM` has a bunch of checks to prevent unauthorised attacks.

### Can I revert to the stock `Preloader` image?

Of course, at your own risk :P.

## Flashing process

* Download the correct `Preloader` image:

!!! note
    LineageOS for `xaga` is based on HyperOS firmware.

| Version | Download |
| :------ | -------: |
| HyperOS | [`preloader_aristotle.bin`](assets/preloader_aristotle.bin) |
| MIUI    | [`preloader_xaga.bin`](assets/preloader_xaga.bin) |

* Reboot your device into fastboot mode by holding down the appropriate key combination.
* Open a `ADB & Fastboot tools` window on your PC and flash the `Preloader` image you downloaded before.

``` bash
# Mention the path of the image before running the commands
# Ex: fastboot flash preloader1 /home/itsvixano/xaga/preloader_xaga.bin
$ fastboot flash preloader1 <preloader_xaga>.bin
$ fastboot flash preloader2 <preloader_xaga>.bin
```

* Reboot your device by holding the `Power` button.
* You are good to go :D

## How to unbrick (with the engineering `Preloader` image)

So you have managed to brick your device and you have previously flashed the engineering `Preloader` image? You can easily restore it by following  these simple steps:

!!! warning
    Avoid using the `Firmware Upgrade` option, it will lock the bootloader.

    Never use the `Format all + Download` option, it will **ERASE** the whole device UFS (including IMEIs, mac addresses, and more).

    Make sure you are flashing with the `Download Only` option.

    **Always make a backup of your partitions**

* Download the Fastboot ROM of your choice.
* Download the custom [`xaga_MT6895_Android_scatter.xml`](assets/xaga_MT6895_Android_scatter.xml){:download="xaga_MT6895_Android_scatter.xml"} file.
* Extract the Fastboot ROM and replace the file `images/MT6895_Android_scatter.xml` with `xaga_MT6895_Android_scatter.xml`.
* Turn off your device.
* Open `SP Flash tool V6`.
* Load the stock Fastboot ROM of your choice by pressing the `Download XML` button and selecting the `images/download_agent/flash.xml` file.
* Double check if you are using the `Download Only` option.
* Press the `Download` button.
* Connect the device to your PC (if it doesn't detect, press and hold the `Power` button for 8-10 seconds).
* The flash process should start.

You can get the latest `SP Flash tool V6` from [here](https://spflashtools.com/v6) and the latest `xaga` Fastboot ROMs from here: (make sure you choose the right MIUI / HyperOS Fastboot ROM for your device :D).

| Version | Download |
| :------ | -------: |
| HyperOS | [`xmfirmwareupdater.com`](https://xmfirmwareupdater.com/hyperos/xaga/) |
| MIUI    | [`xmfirmwareupdater.com`](https://xmfirmwareupdater.com/miui/xaga/) |
